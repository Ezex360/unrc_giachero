fun {SolveOne F}
   L={Solve F}
in
   if L==nil then nil else [L.1] end
end
fun {SolveAll F}
   L = {Solve F}
   proc {TouchAll L}
      if L==nil then skip else {TouchAll L.2} end
   end
in
   {TouchAll L}
   L
end
fun {Digito}
   choice 0 [] 1 [] 2 [] 3 [] 4 [] 5 [] 6 [] 7 [] 8 [] 9 end
end
{Browse {SolveAll Digito}}