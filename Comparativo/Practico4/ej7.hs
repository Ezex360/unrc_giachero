pares = [2,4..]
menores_cien :: [Integer] -> [Integer]
menores_cien (x:xs) | x <=100 = x : menores_cien xs
		    | otherwise = []
cien_pares = menores_cien pares
