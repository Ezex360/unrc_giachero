;Elevar X con un exponente Y
(defun power (x y) (if (= y 0) 1 (* x (power x (1- y))))) 
;Funcion cuadratica menos 1
(defun square (x) (- (* (- x 2) x) 1))
;Devuelve la longitud de una lista
(defun elength (L) (if (null L) 0 (+ 1 (elength (cdr L)))))
;Factorial
(defun factorial (x) (if (= x 1) 1 (* x (factorial (- x 1)))))
;Fibonacci
(defun fib (x) (if (= x 0) 0 (if (= x 1) 1 (+ (fib (- x 1)) (fib (- x 2)) ))))
;Biseccion
(defun biseccion (e f a b) 
  (if (or (< (abs (- a b)) e) (= (funcall f (/ (+ a b) 2))) 0) 
    (/ (+ a b) 2)
    (if (< (* (funcall f (/ (+ a b) 2)) (funcall f (a))) 0)
      (biseccion (e f a (/ (+ a b) 2)))
      (biseccion (e f (/ (+ a b) 2) b ))
     )
   )
)
;Ejemplo : (biseccion 0 'square 1 3) = 2.

