functor
import
    Browser 
define

local Max A B C in
    fun {Max X Y}
        if X>=Y then X else Y end
    end
    A = 3
    B = 2
    {Browser.browse {Max A B}}
end

end
