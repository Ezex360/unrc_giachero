functor
import
    Browser 
define

local X in
    local Y in
	X = person(name:"George" age:25)
	Y = person(name:"George" age:26)
	X = Y
    end
    {Browser.browse Y}
end

end
