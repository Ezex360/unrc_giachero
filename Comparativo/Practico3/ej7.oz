functor
import
    Browser 
define

local X in
    X = 1
    local P in
        P = proc {$ Y}
            local P, A in
            P = proc {$ Z} Z=10 end
                {P A}
            {Browser.browse A+Y}
            end
        end
        {P X}
    end
end

end
