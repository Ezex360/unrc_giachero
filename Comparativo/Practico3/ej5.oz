functor
import
    Browser 
define

local X Y in
    X = 1
    Y = 2 + X
    if X > Y then
        {Browser.browse X}
    else
        {Browser.browse Y}
    end
end

end
