declare Revers in
fun {Revers L}
   local C in
      {NewCell nil C}
      for X in L do
	 C:=X|@C
      end
      @C
   end
end
local T in
   T = {Reverse [1 2 3]}
   {Browse T}
end


  
