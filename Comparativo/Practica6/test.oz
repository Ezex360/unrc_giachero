local X Z P in
   local Y in
      X = 1
      P = 2
      {NewCell X Y}
      {Browse @Y}
      {Exchange Y Z P}
      {Browse @Y}
      {Browse Z}
   end
end
