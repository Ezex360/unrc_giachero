#include <stdio.h>

void main(){
	int p,q;
	p=10;
	q=10;
	int matrix [p][q];	
	for (int i=0;i<p;i++){
		for(int j=0;j<q;j++){
			matrix [i][j]=i+j;
		}
	}
	for (int i=0;i<p;i++){
		for(int j=0;j<q;j++){
			printf("%d ",matrix [i][j]);
		}
		printf("\n");
	}

	int vector [p];
	int *vec=&vector[0];
	int *mat=&matrix[0][0]; 
	int row=-1;
	for (int i=0;i<p;i++){
		*(vec+i)=0;				
	}
	for (int i=0;i<p*q;i++){
		if(i % q == 0)
			row++;
		*(vec+row)+=*(mat+i);				
	}
	printf("Sumatorias \n");	
	for (int i=0;i<p;i++){
		printf("%d ",*(vec+i));
	}
	printf("\n");	


}

