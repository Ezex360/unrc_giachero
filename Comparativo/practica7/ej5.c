#include <stdio.h>

void main(){
	int p,q;
	p=10;
	q=10;
	int matrix [p][q];	
	for (int i=0;i<p;i++){
		for(int j=0;j<q;j++){
			matrix [i][j]=i+j;
		}
	}
	for (int i=0;i<p;i++){
		for(int j=0;j<q;j++){
			printf("%d ",matrix [i][j]);
		}
		printf("\n");
	}

	int row,sum,sum_aux;
	sum=0;
	sum_aux=0;
	for (int i=0;i<p;i++){
		if (sum < sum_aux){
			sum=sum_aux;
			row=i;
		}
		sum_aux=0;
		for(int j=0;j<q;j++){
			sum_aux+= matrix [i][j];
		}
	}
	int *pointer;
	pointer = &matrix [row][0];
	printf("La fila que mas suma es: %d, con un valor de: %d \n",row,sum);
	printf("puntero: %d\n",*pointer);
	//*pointer += 50 ;	
	//printf("puntero: %d, matriz[%d,0] = %d \n",*pointer,row,matrix[row][0]);


}

